import unittest
import re
from datetime import datetime 

DEBUG = 0

""" 
Author: Johnny Shen

how to run? execute following command in the folder that contains the source code file:
> python3 remove_paran_iteration_new.py

general algo logic description:
- use a single stack to track each char in the input str;
- 'lookback' for the last item in the stack, if it's the close paran ')', consolidate
the items in the stack until the matching open paran '(';
- the consolidate method will store the lowest operator pri in a dict h.
- compare the lowest operator pri with the operator before '(' and after ')' to deicde if need to keep the parans, or remove them as unnecessary;
- pay attention to skip the neg numbers, and retain the extra white spaces; 

performance test:
- 1000 times run avg (s) on local pc:  0:00:00.013

"""

def remove_unnecessary_paran(input):
    stack = []
    temp = "" 
    h = {}
    operator_pri = { "+":1, "-":2, "*":3, "/":4, "?":5 }

    def consolidate():
        if not stack: return ""
        str_in_paran = ""
        lowest_operator = "?"
        left_paran_space = ""
        while stack:
            item = stack.pop()
            if item[-1] == "(":    
                left_paran_space = item
                left_paran_space = left_paran_space[:-1]
                break
            else:
                str_in_paran = item + str_in_paran
                # skip the neg number
                if item[-1] == "-" and stack and stack[-1][-1] in "(+-*/":
                    continue

                if item[-1] in operator_pri:
                    if operator_pri[item[-1]] < operator_pri[lowest_operator]:
                        lowest_operator = item[-1]

        if str_in_paran not in h:
            h[str_in_paran] = lowest_operator
        
        if DEBUG: print ("str_in_paran=", str_in_paran)
        # return a tuple
        return (left_paran_space, str_in_paran)


    input = "("+input+")"
    for i in range(1, len(input)):
        curr = input[i]
        if curr == " ":
            temp += curr
        else: # curr is not empty
            # if previous char is a ")"
            if stack and stack[-1][-1]==")":
                # consolidate
                right_paran_space = stack.pop() # pop the ")" from stack
                right_paran_space = right_paran_space[:-1] # remove ")" from the str

                left_paran_space, consolidated_str = consolidate()
                 
                need_paran = False
                # compare the operator before ()
                if stack and stack[-1][-1] in "+-*/":
                    # special case for '*'
                    if stack[-1][-1] == "*":
                        if operator_pri["*"] > operator_pri[h[consolidated_str]]:
                            need_paran = True 
                    else:
                        if operator_pri[stack[-1][-1]] > 1 and operator_pri[stack[-1][-1]] >= operator_pri[h[consolidated_str]]:
                            need_paran = True     
                # compare the operator after ()
                if curr in "*/" and h[consolidated_str] in "+-":
                    need_paran = True

                # retain the original extra white space before paran
                if need_paran:
                    left_paran_space += "("
                    right_paran_space += ")"

                consolidated_str = left_paran_space + consolidated_str + right_paran_space
                stack.append(consolidated_str)
                
            temp += curr
            stack.append(temp)
            temp = ""

        if DEBUG:
            print ("curr=", curr)
            print ("temp=", temp)
            print ("h=", h)
            print ("stack=", stack)
            print ("------")
    # pop the last unnecessary ")"
    stack.pop()
    return "".join(stack)

DEBUG=0
print (remove_unnecessary_paran("(1)/(2)/(3/3)"))
exit()


# performance test
start=datetime.now()
for i in range(1000):
    remove_unnecessary_paran("(((4-2-(3+2*6)/(3+5*2)+5))-(3+5))")
print ("1000 times run last: ", datetime.now()-start)


class TestRemoveUnnecessaryParan(unittest.TestCase):

    def test_cases(self):
        # test on various complex cases
        self.assertEqual(remove_unnecessary_paran("(((4-2-(3+2*6)/(3+5*2)+5))-(3+5))"), "4-2-(3+2*6)/(3+5*2)+5-(3+5)")
        self.assertEqual(remove_unnecessary_paran("4-2-(3+2*6)/(3+5*2)+5-(3+5)"), "4-2-(3+2*6)/(3+5*2)+5-(3+5)")
        self.assertEqual(remove_unnecessary_paran("(3+(2/4-2))"), "3+2/4-2")
        self.assertEqual(remove_unnecessary_paran("1*(2+(3*(4+5)))"), "1*(2+3*(4+5))")
        self.assertEqual(remove_unnecessary_paran("2 + (3 / -5)"), "2 + 3 / -5")
        self.assertEqual(remove_unnecessary_paran("-x+( y+ -z)+(t+(v+  w))"), "-x+ y+ -z+t+v+  w")
        self.assertEqual(remove_unnecessary_paran("((((a+b))))"), "a+b")
        self.assertEqual(remove_unnecessary_paran("3/((((a+b))))"), "3/(a+b)")
        # basic paran pri test
        self.assertEqual(remove_unnecessary_paran("2-(2+3)"), "2-(2+3)")
        # paran in the end
        self.assertEqual(remove_unnecessary_paran("2+(2+3)"), "2+2+3")
        self.assertEqual(remove_unnecessary_paran("2*(2+3)"), "2*(2+3)")
        self.assertEqual(remove_unnecessary_paran("2*(2/3)"), "2*2/3")
        # paran in the beginning
        self.assertEqual(remove_unnecessary_paran("(2-2)/3"), "(2-2)/3")
        self.assertEqual(remove_unnecessary_paran("(2*2-3)"), "2*2-3")
        # with no paran
        self.assertEqual(remove_unnecessary_paran("2*2-3"), "2*2-3")
        # test on beginning with neg num
        self.assertEqual(remove_unnecessary_paran("-3+2/(4-2)"), "-3+2/(4-2)")
        # test on neg num in paran
        self.assertEqual(remove_unnecessary_paran("(-3+2)/(4-2)"), "(-3+2)/(4-2)")
        # test on no paran at all
        self.assertEqual(remove_unnecessary_paran("2*2/3"), "2*2/3")
        # test on multi level operaters and paran in the middle
        self.assertEqual(remove_unnecessary_paran("2+(2+3)/5"), "2+(2+3)/5")
        # test on single num in paran
        self.assertEqual(remove_unnecessary_paran("(-2)*(2)/(3)"), "-2*2/3")
        self.assertEqual(remove_unnecessary_paran("  (  -2 ) * (  2) /  (-3  )"), "    -2  *   2 /  -3  ")
        self.assertEqual(remove_unnecessary_paran("-5 + (1/(-14 + 1/(4)))"), "-5 + 1/(-14 + 1/4)")
        # single operants
        self.assertEqual(remove_unnecessary_paran("1"), "1")
        self.assertEqual(remove_unnecessary_paran("(1)"), "1")
        self.assertEqual(remove_unnecessary_paran("(-1)"), "-1")
        self.assertEqual(remove_unnecessary_paran("1+2"), "1+2")
        self.assertEqual(remove_unnecessary_paran("1-2"), "1-2")
        self.assertEqual(remove_unnecessary_paran("1*2"), "1*2")
        self.assertEqual(remove_unnecessary_paran("1/2"), "1/2")
        self.assertEqual(remove_unnecessary_paran("(1+2)"), "1+2")
        self.assertEqual(remove_unnecessary_paran("(1-2)"), "1-2")
        self.assertEqual(remove_unnecessary_paran("(1*2)"), "1*2")
        self.assertEqual(remove_unnecessary_paran("(1/2)"), "1/2")
        self.assertEqual(remove_unnecessary_paran("1+2-3"), "1+2-3")
        self.assertEqual(remove_unnecessary_paran("1-2-3"), "1-2-3")
        self.assertEqual(remove_unnecessary_paran("1*2*3"), "1*2*3")
        self.assertEqual(remove_unnecessary_paran("1/2/3"), "1/2/3")
        self.assertEqual(remove_unnecessary_paran("1+2*3"), "1+2*3")
        self.assertEqual(remove_unnecessary_paran("1-2/3"), "1-2/3")
        self.assertEqual(remove_unnecessary_paran("1*2+3"), "1*2+3")
        self.assertEqual(remove_unnecessary_paran("(1+2)-3"), "1+2-3")
        self.assertEqual(remove_unnecessary_paran("(1-2)-3"), "1-2-3")
        self.assertEqual(remove_unnecessary_paran("(1*2)*3"), "1*2*3")
        self.assertEqual(remove_unnecessary_paran("(1/2)/3"), "1/2/3")
        self.assertEqual(remove_unnecessary_paran("(1+2)*3"), "(1+2)*3")
        self.assertEqual(remove_unnecessary_paran("(1-2)/3"), "(1-2)/3")
        self.assertEqual(remove_unnecessary_paran("(1*2)+3"), "1*2+3")
        self.assertEqual(remove_unnecessary_paran("1/(2+3)"), "1/(2+3)")
        self.assertEqual(remove_unnecessary_paran("1+(2-3)"), "1+2-3")
        self.assertEqual(remove_unnecessary_paran("1-(2-3)"), "1-(2-3)")
        self.assertEqual(remove_unnecessary_paran("1*(2*3)"), "1*2*3")
        self.assertEqual(remove_unnecessary_paran("1/(2/3)"), "1/(2/3)")
        self.assertEqual(remove_unnecessary_paran("1/(2*3)"), "1/(2*3)")
        self.assertEqual(remove_unnecessary_paran("1+(2*3)"), "1+2*3")
        self.assertEqual(remove_unnecessary_paran("1-(2/3)"), "1-2/3")
        self.assertEqual(remove_unnecessary_paran("1*(2+3)"), "1*(2+3)")
        self.assertEqual(remove_unnecessary_paran("1/(2-3)"), "1/(2-3)")
        self.assertEqual(remove_unnecessary_paran("1*(2/3)"), "1*2/3")
        self.assertEqual(remove_unnecessary_paran("((1+2))-3"), "1+2-3")
        self.assertEqual(remove_unnecessary_paran("((1-2))-3"), "1-2-3")
        self.assertEqual(remove_unnecessary_paran("((1*2))*3"), "1*2*3")
        self.assertEqual(remove_unnecessary_paran("((1/2))/3"), "1/2/3")
        self.assertEqual(remove_unnecessary_paran("((1+2))*3"), "(1+2)*3")
        self.assertEqual(remove_unnecessary_paran("((1-2))/3"), "(1-2)/3")
        self.assertEqual(remove_unnecessary_paran("((1*2))+3"), "1*2+3")
        self.assertEqual(remove_unnecessary_paran("1/((2+3))"), "1/(2+3)")
        self.assertEqual(remove_unnecessary_paran("1+((2-3))"), "1+2-3")
        self.assertEqual(remove_unnecessary_paran("1-((2-3))"), "1-(2-3)")
        self.assertEqual(remove_unnecessary_paran("1*((2*3))"), "1*2*3")
        self.assertEqual(remove_unnecessary_paran("1/(((2/3)))"), "1/(2/3)")
        self.assertEqual(remove_unnecessary_paran("1/((2*3))"), "1/(2*3)")
        self.assertEqual(remove_unnecessary_paran("1+((2*3))"), "1+2*3")
        self.assertEqual(remove_unnecessary_paran("1-((2/3))"), "1-2/3")
        self.assertEqual(remove_unnecessary_paran("1*((2+3))"), "1*(2+3)")
        self.assertEqual(remove_unnecessary_paran("1/((2+3))"), "1/(2+3)")
        self.assertEqual(remove_unnecessary_paran("1*((2/3))"), "1*2/3")
        self.assertEqual(remove_unnecessary_paran("1*(((2/3)))"), "1*2/3")
        self.assertEqual(remove_unnecessary_paran("1+2-3+4"), "1+2-3+4")
        self.assertEqual(remove_unnecessary_paran("1-2-3+4"), "1-2-3+4")
        self.assertEqual(remove_unnecessary_paran("1*2*3+4"), "1*2*3+4")
        self.assertEqual(remove_unnecessary_paran("1/2/3+4"), "1/2/3+4")
        self.assertEqual(remove_unnecessary_paran("1+2*3+4"), "1+2*3+4")
        self.assertEqual(remove_unnecessary_paran("1-2/3+4"), "1-2/3+4")
        self.assertEqual(remove_unnecessary_paran("1*2+3+4"), "1*2+3+4")
        self.assertEqual(remove_unnecessary_paran("1+2-3*4"), "1+2-3*4")
        self.assertEqual(remove_unnecessary_paran("1-2-3*4"), "1-2-3*4")
        self.assertEqual(remove_unnecessary_paran("1*2*3*4"), "1*2*3*4")
        self.assertEqual(remove_unnecessary_paran("1/2/3*4"), "1/2/3*4")
        self.assertEqual(remove_unnecessary_paran("1+2*3*4"), "1+2*3*4")
        self.assertEqual(remove_unnecessary_paran("1-2/3*4"), "1-2/3*4")
        self.assertEqual(remove_unnecessary_paran("1*2+3*4"), "1*2+3*4")
        self.assertEqual(remove_unnecessary_paran("1+2-3/4"), "1+2-3/4")
        self.assertEqual(remove_unnecessary_paran("1-2-3/4"), "1-2-3/4")
        self.assertEqual(remove_unnecessary_paran("1*2*3/4"), "1*2*3/4")
        self.assertEqual(remove_unnecessary_paran("1/2/3/4"), "1/2/3/4")
        self.assertEqual(remove_unnecessary_paran("1+2*3/4"), "1+2*3/4")
        self.assertEqual(remove_unnecessary_paran("1-2/3/4"), "1-2/3/4")
        self.assertEqual(remove_unnecessary_paran("1*2+3/4"), "1*2+3/4")        
        self.assertEqual(remove_unnecessary_paran("(1+2)-3+4"), "1+2-3+4")
        self.assertEqual(remove_unnecessary_paran("(1-2)-3+4"), "1-2-3+4")
        self.assertEqual(remove_unnecessary_paran("(1*2)*3+4"), "1*2*3+4")
        self.assertEqual(remove_unnecessary_paran("(1/2)/3+4"), "1/2/3+4")
        self.assertEqual(remove_unnecessary_paran("(1+2)*3+4"), "(1+2)*3+4")
        self.assertEqual(remove_unnecessary_paran("(1-2)/3+4"), "(1-2)/3+4")
        self.assertEqual(remove_unnecessary_paran("(1*2)+3+4"), "1*2+3+4")
        self.assertEqual(remove_unnecessary_paran("1+(2-3)+4"), "1+2-3+4")
        self.assertEqual(remove_unnecessary_paran("1-(2-3)+4"), "1-(2-3)+4")
        self.assertEqual(remove_unnecessary_paran("1*(2*3)+4"), "1*2*3+4")
        self.assertEqual(remove_unnecessary_paran("1/(2/3)+4"), "1/(2/3)+4")
        self.assertEqual(remove_unnecessary_paran("1*(2/3)+4"), "1*2/3+4")
        self.assertEqual(remove_unnecessary_paran("1-(2/3)+4"), "1-2/3+4")
        self.assertEqual(remove_unnecessary_paran("1*(2/3)*4"), "1*2/3*4")
        self.assertEqual(remove_unnecessary_paran("1+(2/3)*4"), "1+2/3*4")
        self.assertEqual(remove_unnecessary_paran("1+(2*3)+4"), "1+2*3+4")
        self.assertEqual(remove_unnecessary_paran("1-(2/3)+4"), "1-2/3+4")
        self.assertEqual(remove_unnecessary_paran("1*(2+3)+4"), "1*(2+3)+4")
        self.assertEqual(remove_unnecessary_paran("1+2-(3+4)"), "1+2-(3+4)")
        self.assertEqual(remove_unnecessary_paran("1-2-(3+4)"), "1-2-(3+4)")
        self.assertEqual(remove_unnecessary_paran("1*2*(3+4)"), "1*2*(3+4)")
        self.assertEqual(remove_unnecessary_paran("1/2/(3+4)"), "1/2/(3+4)")
        self.assertEqual(remove_unnecessary_paran("1+2*(3+4)"), "1+2*(3+4)")
        self.assertEqual(remove_unnecessary_paran("1-2/(3+4)"), "1-2/(3+4)")
        self.assertEqual(remove_unnecessary_paran("1*2+(3+4)"), "1*2+3+4")
        self.assertEqual(remove_unnecessary_paran("(1+2-3)+4"), "1+2-3+4")
        self.assertEqual(remove_unnecessary_paran("(1-2-3)+4"), "1-2-3+4")
        self.assertEqual(remove_unnecessary_paran("(1*2*3)+4"), "1*2*3+4")
        self.assertEqual(remove_unnecessary_paran("(1/2/3)+4"), "1/2/3+4")
        self.assertEqual(remove_unnecessary_paran("(1+2*3)+4"), "1+2*3+4")
        self.assertEqual(remove_unnecessary_paran("(1-2/3)+4"), "1-2/3+4")
        self.assertEqual(remove_unnecessary_paran("(1*2+3)+4"), "1*2+3+4")
        self.assertEqual(remove_unnecessary_paran("1+(2-3+4)"), "1+2-3+4")
        self.assertEqual(remove_unnecessary_paran("1-(2-3+4)"), "1-(2-3+4)")
        self.assertEqual(remove_unnecessary_paran("1*(2*3+4)"), "1*(2*3+4)")
        self.assertEqual(remove_unnecessary_paran("1/(2/3+4)"), "1/(2/3+4)")
        self.assertEqual(remove_unnecessary_paran("1+(2*3+4)"), "1+2*3+4")
        self.assertEqual(remove_unnecessary_paran("1-(2/3+4)"), "1-(2/3+4)")
        self.assertEqual(remove_unnecessary_paran("1*(2+3+4)"), "1*(2+3+4)")
        self.assertEqual(remove_unnecessary_paran("(1*2)+(3+4)"), "1*2+3+4")
        self.assertEqual(remove_unnecessary_paran("(1+2)+(3+4)"), "1+2+3+4")
        self.assertEqual(remove_unnecessary_paran("(1*2)-(3+4)"), "1*2-(3+4)")
        self.assertEqual(remove_unnecessary_paran("(1+2)-(3+4)"), "1+2-(3+4)")
        self.assertEqual(remove_unnecessary_paran("(1*2)/(3+4)"), "1*2/(3+4)")
        self.assertEqual(remove_unnecessary_paran("1/2/3/4"), "1/2/3/4")
        self.assertEqual(remove_unnecessary_paran("(1/2)/3/4"), "1/2/3/4")
        self.assertEqual(remove_unnecessary_paran("1/2/(3/4)"), "1/2/(3/4)")
        self.assertEqual(remove_unnecessary_paran("1/2/(((3/4)))"), "1/2/(3/4)")
        self.assertEqual(remove_unnecessary_paran("(1/2/3)/4"), "1/2/3/4")
        self.assertEqual(remove_unnecessary_paran("1/(2/3)/4"), "1/(2/3)/4")
        self.assertEqual(remove_unnecessary_paran("(1/2)/(3/4)"), "1/2/(3/4)")
        self.assertEqual(remove_unnecessary_paran("((1+2))-(3-4)"), "1+2-(3-4)")
        self.assertEqual(remove_unnecessary_paran("((1+2))*(3-4)"), "(1+2)*(3-4)")
        self.assertEqual(remove_unnecessary_paran("((1+2))-((3-4))"), "1+2-(3-4)")
        self.assertEqual(remove_unnecessary_paran("((1+2))*((3-4))"), "(1+2)*(3-4)")
        self.assertEqual(remove_unnecessary_paran("(((1+2))-((3-4)))"), "1+2-(3-4)")
        self.assertEqual(remove_unnecessary_paran("(((1+2)))*(((3-4)))"), "(1+2)*(3-4)")
        self.assertEqual(remove_unnecessary_paran("1+((2-3))-4"), "1+2-3-4")
        self.assertEqual(remove_unnecessary_paran("1+((2*3))-4"), "1+2*3-4")
        self.assertEqual(remove_unnecessary_paran("1+((2-3)-4)"), "1+2-3-4")
        self.assertEqual(remove_unnecessary_paran("1/((2-3)-4)"), "1/(2-3-4)")
        self.assertEqual(remove_unnecessary_paran("1*(2-(3-4))"), "1*(2-(3-4))")
        self.assertEqual(remove_unnecessary_paran("((1+2)-3)-4"), "1+2-3-4")
        self.assertEqual(remove_unnecessary_paran("((1+2)-3)/4"), "(1+2-3)/4")
        self.assertEqual(remove_unnecessary_paran("1+(2-(3-4))"), "1+2-(3-4)")
        self.assertEqual(remove_unnecessary_paran("1-(2+(3-4))"), "1-(2+3-4)")
        self.assertEqual(remove_unnecessary_paran("1/(2*(3-4))"), "1/(2*(3-4))")
        self.assertEqual(remove_unnecessary_paran("1*(2*(3-4))"), "1*2*(3-4)")
        self.assertEqual(remove_unnecessary_paran("(1/2)*(3-4)"), "1/2*(3-4)")
        self.assertEqual(remove_unnecessary_paran("((1/2)*3)-4"), "1/2*3-4")
        self.assertEqual(remove_unnecessary_paran("1-(2*(3-4))"), "1-2*(3-4)")
        self.assertEqual(remove_unnecessary_paran("1+(2-(3-4))"), "1+2-(3-4)")
        self.assertEqual(remove_unnecessary_paran("(1+2)/3-(4-5)"), "(1+2)/3-(4-5)")
        self.assertEqual(remove_unnecessary_paran("(1+2)/(3-(4-5))"), "(1+2)/(3-(4-5))")
        self.assertEqual(remove_unnecessary_paran("1+(2-3)-(4-5)"), "1+2-3-(4-5)")
        self.assertEqual(remove_unnecessary_paran("1+(((2-3)-(4-5)))"), "1+2-3-(4-5)")
        self.assertEqual(remove_unnecessary_paran("1+(2-3)-(((4-5)))"), "1+2-3-(4-5)")
        self.assertEqual(remove_unnecessary_paran("1/(((2-3)))-(4-5)"), "1/(2-3)-(4-5)")
        self.assertEqual(remove_unnecessary_paran("1+(2-3)-(2-3)"), "1+2-3-(2-3)")
        self.assertEqual(remove_unnecessary_paran("1+(2-3)/(2-3)"), "1+(2-3)/(2-3)")
        self.assertEqual(remove_unnecessary_paran("((1+(2-3)))/(2-3)"), "(1+2-3)/(2-3)")
        # with neg numbers
        self.assertEqual(remove_unnecessary_paran("(-1*2)+(3+-4)"), "-1*2+3+-4")
        self.assertEqual(remove_unnecessary_paran("(1+-2)+(3+-4)"), "1+-2+3+-4")
        self.assertEqual(remove_unnecessary_paran("(-1*-2)-(-3+4)"), "-1*-2-(-3+4)")
        self.assertEqual(remove_unnecessary_paran("(1+-2)-(3+4)"), "1+-2-(3+4)")
        self.assertEqual(remove_unnecessary_paran("(1*-2)/(-3+4)"), "1*-2/(-3+4)")
        self.assertEqual(remove_unnecessary_paran("-1/2/3/4"), "-1/2/3/4")
        self.assertEqual(remove_unnecessary_paran("(-1/2)/3/4"), "-1/2/3/4")
        self.assertEqual(remove_unnecessary_paran("1/-2/(3/4)"), "1/-2/(3/4)")
        self.assertEqual(remove_unnecessary_paran("(-1/2/-3)/4"), "-1/2/-3/4")
        self.assertEqual(remove_unnecessary_paran("1/(-2/3)/-4"), "1/(-2/3)/-4")
        self.assertEqual(remove_unnecessary_paran("(1/2)/(3/-4)"), "1/2/(3/-4)")
        # with extra spaces
        self.assertEqual(remove_unnecessary_paran("( 1* 2)+(3+4)"), " 1* 2+3+4")
        self.assertEqual(remove_unnecessary_paran("(1+2)+   (3+4)"), "1+2+   3+4")
        self.assertEqual(remove_unnecessary_paran("(1*2)-(3+  4)"), "1*2-(3+  4)")
        self.assertEqual(remove_unnecessary_paran("  (1+2)-(3+4)"), "  1+2-(3+4)")
        self.assertEqual(remove_unnecessary_paran("(1*2)/(3+4)"), "1*2/(3+4)")
        self.assertEqual(remove_unnecessary_paran("1/ 2/3/ 4"), "1/ 2/3/ 4")
        self.assertEqual(remove_unnecessary_paran("(1/2)/  3/4"), "1/2/  3/4")
        self.assertEqual(remove_unnecessary_paran("1/2/(( 3 / 4)  )"), "1/2/( 3 / 4  )")
        self.assertEqual(remove_unnecessary_paran("(  -1/ 2/3)/4"), "  -1/ 2/3/4")
        self.assertEqual(remove_unnecessary_paran("1/(2/3)/  -4"), "1/(2/3)/  -4")
        self.assertEqual(remove_unnecessary_paran("((1/2))/   (3/4)"), "1/2/   (3/4)")


unittest.main()